<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\Crit
 *
 * @ORM\Table(name="crit")
 * @ORM\Entity
 */
class Crit
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
    
    
    /**
     * @var string $view
     *
     * @ORM\Column(name="view", type="string", length=255, nullable=true)
     */
    private $view;

    /**
     * @var string $descrition
     *
     * @ORM\Column(name="descrition", type="text", nullable=true)
     */
    private $descrition;
    
    
    /**
     * @var string $shared
     *
     * @ORM\Column(name="shared", type="text", nullable=true)
     */
    private $shared;
    
    
    /**
     * @var string $sharedmess
     *
     * @ORM\Column(name="sharedmess", type="text", nullable=true)
     */
    private $sharedmess;

    /**
     * @var \DateTime $expiration
     *
     * @ORM\Column(name="expiration", type="datetime", nullable=true)
     */
    private $expiration;

    /**
     * @var integer $isActive
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    /**
     * @var string $linkUrl
     *
     * @ORM\Column(name="link_url", type="text", nullable=true)
     */
    private $linkUrl;

    /**
     * @var integer $isPrivate
     *
     * @ORM\Column(name="is_private", type="integer", nullable=true)
     */
    private $isPrivate;


   /**
     * @var integer $isAccess
     *
     * @ORM\Column(name="is_access", type="integer", nullable=true)
     */
    private $isAccess;
    
    
    /**
     * @var integer $IsResponse
     *
     * @ORM\Column(name="is_response", type="integer", nullable=true)
     */
    private $isResponse;
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var WorkItem
     *
     * @ORM\ManyToOne(targetEntity="WorkItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="work_item_id", referencedColumnName="id")
     * })
     */
    private $workItem;

   /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * @var integer $responcescount
     *
     * @ORM\Column(name="responcescount", type="integer", nullable=true)
     */
    private $responcescount;
    
    
    /**
     * @var integer $avgratingcount
     *
     * @ORM\Column(name="avgratingcount", type="integer", nullable=true)
     */
    private $avgratingcount;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Crit
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    
    /**
     * Set view
     *
     * @param string $view
     * @return Crit
     */
    public function setView($view)
    {
        $this->view = $view;
    
        return $this;
    }

    /**
     * Get view
     *
     * @return string 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set descrition
     *
     * @param string $descrition
     * @return Crit
     */
    public function setDescrition($descrition)
    {
        $this->descrition = $descrition;
    
        return $this;
    }

    /**
     * Get descrition
     *
     * @return string 
     */
    public function getDescrition()
    {
        return $this->descrition;
    }
    
    
    /**
     * Set shared
     *
     * @param string $shared
     * @return Crit
     */
    public function setShared($shared)
    {
        $this->shared = $shared;
    
        return $this;
    }

    /**
     * Get shared
     *
     * @return string 
     */
    public function getShared()
    {
        return $this->shared;
    }


   /**
     * Set sharedmess
     *
     * @param string $sharedmess
     * @return Crit
     */
    public function setSharedmess($sharedmess)
    {
        $this->sharedmess = $sharedmess;
    
        return $this;
    }

    /**
     * Get sharedmess
     *
     * @return string 
     */
    public function getSharedmess()
    {
        return $this->sharedmess;
    }

    /**
     * Set expiration
     *
     * @param \DateTime $expiration
     * @return Crit
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    
        return $this;
    }

    /**
     * Get expiration
     *
     * @return \DateTime 
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return Crit
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set linkUrl
     *
     * @param string $linkUrl
     * @return Crit
     */
    public function setLinkUrl($linkUrl)
    {
        $this->linkUrl = $linkUrl;
    
        return $this;
    }

    /**
     * Get linkUrl
     *
     * @return string 
     */
    public function getLinkUrl()
    {
        return $this->linkUrl;
    }

    /**
     * Set isPrivate
     *
     * @param integer $isPrivate
     * @return Crit
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;
    
        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return integer 
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }


    /**
     * Set isAccess
     *
     * @param integer $isAccess
     * @return Crit
     */
    public function setIsAccess($isAccess)
    {
        $this->isAccess = $isAccess;
    
        return $this;
    }

    /**
     * Get isAccess
     *
     * @return integer 
     */
    public function getIsAccess()
    {
        return $this->isAccess;
    }
    
    
     /**
     * Set isResponse
     *
     * @param integer $isResponse
     * @return Crit
     */
    public function setIsResponse($isResponse)
    {
        $this->isResponse = $isResponse;
    
        return $this;
    }

    /**
     * Get isResponse
     *
     * @return integer 
     */
    public function getIsResponse()
    {
        return $this->isResponse;
    }
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Crit
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Crit
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set workItem
     *
     * @param Acme\SkoodioBundle\Entity\WorkItem $workItem
     * @return Crit
     */
    public function setWorkItem(\Acme\SkoodioBundle\Entity\WorkItem $workItem = null)
    {
        $this->workItem = $workItem;
    
        return $this;
    }

    /**
     * Get workItem
     *
     * @return Acme\SkoodioBundle\Entity\WorkItem 
     */
    public function getWorkItem()
    {
        return $this->workItem;
    }
    
    
    
    
    
    /**
     * Set user
     *
     * @param Acme\SkoodioBundle\Entity\User $user
     * @return Crit
     */
    public function setUser(\Acme\SkoodioBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Acme\SkoodioBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    
     /**
     * Set responcescount
     *
     * @param integer $responcescount
     * @return Crit
     */
    public function setResponcescount($responcescount)
    {
        $this->responcescount = $responcescount;
    
        return $this;
    }

    /**
     * Get responcescount
     *
     * @return integer 
     */
    public function getResponcescount()
    {
        return $this->responcescount;
    }
    /**
     * Set avgratingcount
     *
     * @param integer $avgratingcount
     * @return Crit
     */
    public function setAvgratingcount($avgratingcount)
    {
        $this->avgratingcount = $avgratingcount;
    
        return $this;
    }

    /**
     * Get avgratingcount
     *
     * @return integer 
     */
    public function getAvgratingcount()
    {
        return $this->avgratingcount;
    }
}