<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\CritQuestion
 *
 * @ORM\Table(name="crit_question")
 * @ORM\Entity
 */
class CritQuestion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $questionText
     *
     * @ORM\Column(name="question_text", type="text", nullable=true)
     */
    private $questionText;

    /**
     * @var integer $isActive
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var Crit
     *
     * @ORM\ManyToOne(targetEntity="Crit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_id", referencedColumnName="id")
     * })
     */
    private $crit;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionText
     *
     * @param string $questionText
     * @return CritQuestion
     */
    public function setQuestionText($questionText)
    {
        $this->questionText = $questionText;
    
        return $this;
    }

    /**
     * Get questionText
     *
     * @return string 
     */
    public function getQuestionText()
    {
        return $this->questionText;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return CritQuestion
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CritQuestion
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CritQuestion
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set crit
     *
     * @param Acme\SkoodioBundle\Entity\Crit $crit
     * @return CritQuestion
     */
    public function setCrit(\Acme\SkoodioBundle\Entity\Crit $crit = null)
    {
        $this->crit = $crit;
    
        return $this;
    }

    /**
     * Get crit
     *
     * @return Acme\SkoodioBundle\Entity\Crit 
     */
    public function getCrit()
    {
        return $this->crit;
    }
}