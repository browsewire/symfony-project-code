<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\Rating
 *
 * @ORM\Table(name="rating")
 * @ORM\Entity
 */
class Rating
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $rating
     *
     * @ORM\Column(name="rating", type="string", length=255, nullable=true)
     */
    private $rating;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var integer $isActive
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;
    
    
     /**
     * @var integer anonymous
     *
     * @ORM\Column(name="anonymous", type="integer", nullable=true)
     */
    private $anonymous;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var Crit
     *
     * @ORM\ManyToOne(targetEntity="Crit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_id", referencedColumnName="id")
     * })
     */
    private $crit;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param string $rating
     * @return Rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    
        return $this;
    }

    /**
     * Get rating
     *
     * @return string 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Rating
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Rating
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return Rating
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }


    /**
     * Set anonymous
     *
     * @param integer $anonymous
     * @return Rating
     */
    public function setAnonymous($anonymous)
    {
        $this->anonymous = $anonymous;
    
        return $this;
    }

    /**
     * Get anonymous
     *
     * @return integer 
     */
    public function getAnonymous()
    {
        return $this->anonymous;
    }
    
    
    /**
     * Set user
     *
     * @param Acme\SkoodioBundle\Entity\User $user
     * @return Rating
     */
    public function setUser(\Acme\SkoodioBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Acme\SkoodioBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set crit
     *
     * @param Acme\SkoodioBundle\Entity\Crit $crit
     * @return Rating
     */
    public function setCrit(\Acme\SkoodioBundle\Entity\Crit $crit = null)
    {
        $this->crit = $crit;
    
        return $this;
    }

    /**
     * Get crit
     *
     * @return Acme\SkoodioBundle\Entity\Crit 
     */
    public function getCrit()
    {
        return $this->crit;
    }
}