<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\Reviewer
 *
 * @ORM\Table(name="reviewer")
 * @ORM\Entity
 */
class Reviewer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $firstName
     *
     * @ORM\Column(name="first_name", type="integer", nullable=true)
     */
    private $firstName;

    /**
     * @var string $lastName
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer $anonymous
     *
     * @ORM\Column(name="anonymous", type="integer", nullable=true)
     */
    private $anonymous;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param integer $firstName
     * @return Reviewer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return integer 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Reviewer
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Reviewer
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set anonymous
     *
     * @param integer $anonymous
     * @return Reviewer
     */
    public function setAnonymous($anonymous)
    {
        $this->anonymous = $anonymous;
    
        return $this;
    }

    /**
     * Get anonymous
     *
     * @return integer 
     */
    public function getAnonymous()
    {
        return $this->anonymous;
    }

    /**
     * Set user
     *
     * @param Acme\SkoodioBundle\Entity\User $user
     * @return Reviewer
     */
    public function setUser(\Acme\SkoodioBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Acme\SkoodioBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}