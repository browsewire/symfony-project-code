<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\WorkItem
 *
 * @ORM\Table(name="work_item")
 * @ORM\Entity
 */
class WorkItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $attachment
     *
     * @ORM\Column(name="attachment", type="text", nullable=true)
     */
    private $attachment;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $lastActivity
     *
     * @ORM\Column(name="last_activity", type="text", nullable=true)
     */
    private $lastActivity;

    /**
     * @var string $link
     *
     * @ORM\Column(name="link", type="text", nullable=true)
     */
    private $link;

    /**
     * @var integer $isActive
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string $access
     *
     * @ORM\Column(name="access", type="string", length=255, nullable=true)
     */
    private $access;

    /**
     * @var WorkItemType
     *
     * @ORM\ManyToOne(targetEntity="WorkItemType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="work_item_type_id", referencedColumnName="id")
     * })
     */
    private $workItemType;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string $view
     *
     * @ORM\Column(name="view", type="string", length=255, nullable=true)
     */
    private $view;

   /**
     * @var string $shared
     *
     * @ORM\Column(name="shared", type="text", nullable=true)
     */
    private $shared;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return WorkItem
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set attachment
     *
     * @param string $attachment
     * @return WorkItem
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    
        return $this;
    }

    /**
     * Get attachment
     *
     * @return string 
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return WorkItem
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lastActivity
     *
     * @param string $lastActivity
     * @return WorkItem
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    
        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return string 
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return WorkItem
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return WorkItem
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return WorkItem
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return WorkItem
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set access
     *
     * @param string $access
     * @return WorkItem
     */
    public function setAccess($access)
    {
        $this->access = $access;
    
        return $this;
    }

    /**
     * Get access
     *
     * @return string 
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set workItemType
     *
     * @param Acme\SkoodioBundle\Entity\WorkItemType $workItemType
     * @return WorkItem
     */
    public function setWorkItemType(\Acme\SkoodioBundle\Entity\WorkItemType $workItemType = null)
    {
        $this->workItemType = $workItemType;
    
        return $this;
    }

    /**
     * Get workItemType
     *
     * @return Acme\SkoodioBundle\Entity\WorkItemType 
     */
    public function getWorkItemType()
    {
        return $this->workItemType;
    }

    /**
     * Set user
     *
     * @param Acme\SkoodioBundle\Entity\User $user
     * @return WorkItem
     */
    public function setUser(\Acme\SkoodioBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Acme\SkoodioBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    
    /**
     * Set view
     *
     * @param string $view
     * @return WorkItem
     */
    public function setView($view)
    {
        $this->view = $view;
    
        return $this;
    }

    /**
     * Get view
     *
     * @return string 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set shared
     *
     * @param string $shared
     * @return WorkItem
     */
    public function setShared($shared)
    {
        $this->shared = $shared;
    
        return $this;
    }

    /**
     * Get shared
     *
     * @return string 
     */
    public function getShared()
    {
        return $this->shared;
    }


}