<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\Annotation
 *
 * @ORM\Table(name="annotation")
 * @ORM\Entity
 */
class Annotation
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CrocoDocFile
     *
     * @ORM\ManyToOne(targetEntity="CrocoDocFile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="croco_doc_id", referencedColumnName="id")
     * })
     */
    private $crocoDoc;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set crocoDoc
     *
     * @param Acme\SkoodioBundle\Entity\CrocoDocFile $crocoDoc
     * @return Annotation
     */
    public function setCrocoDoc(\Acme\SkoodioBundle\Entity\CrocoDocFile $crocoDoc = null)
    {
        $this->crocoDoc = $crocoDoc;
    
        return $this;
    }

    /**
     * Get crocoDoc
     *
     * @return Acme\SkoodioBundle\Entity\CrocoDocFile 
     */
    public function getCrocoDoc()
    {
        return $this->crocoDoc;
    }
}