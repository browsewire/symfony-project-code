<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\Comments
 *
 * @ORM\Table(name="comments")
 * @ORM\Entity
 */
class Comments
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer $isActive
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    /**
     * @var integer $isPrivate
     *
     * @ORM\Column(name="is_private", type="integer", nullable=true)
     */
    private $isPrivate;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var CritQuestion
     *
     * @ORM\ManyToOne(targetEntity="CritQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_question_id", referencedColumnName="id")
     * })
     */
    private $critQuestion;


    /**
     * @var Crit
     *
     * @ORM\ManyToOne(targetEntity="Crit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_id", referencedColumnName="id")
     * })
     */
    private $crit;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Comments
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Comments
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return Comments
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isPrivate
     *
     * @param integer $isPrivate
     * @return Comments
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;
    
        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return integer 
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Comments
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Comments
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set user
     *
     * @param Acme\SkoodioBundle\Entity\User $user
     * @return Comments
     */
    public function setUser(\Acme\SkoodioBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Acme\SkoodioBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set crit
     *
     * @param Acme\SkoodioBundle\Entity\Crit $crit
     * @return Comments
     */
    public function setCrit(\Acme\SkoodioBundle\Entity\Crit $crit = null)
    {
        $this->crit = $crit;
    
        return $this;
    }

    /**
     * Get crit
     *
     * @return Acme\SkoodioBundle\Entity\Crit 
     */
    public function getCrit()
    {
        return $this->crit;
    }
    
    
    /**
     * Set critQuestion
     *
     * @param Acme\SkoodioBundle\Entity\CritQuestion $critQuestion
     * @return Comments
     */
    public function setCritQuestion(\Acme\SkoodioBundle\Entity\CritQuestion $critQuestion = null)
    {
        $this->critQuestion = $critQuestion;
    
        return $this;
    }

    /**
     * Get critQuestion
     *
     * @return Acme\SkoodioBundle\Entity\CritQuestion 
     */
    public function getCritQuestion()
    {
        return $this->critQuestion;
    }
    
}