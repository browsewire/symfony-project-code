<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\CritPremium
 *
 * @ORM\Table(name="crit_premium")
 * @ORM\Entity
 */
class CritPremium
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $workItemId
     *
     * @ORM\Column(name="work_item_id", type="text", nullable=true)
     */
    private $workItemId;

    
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

     /**
     * @var Crit
     *
     * @ORM\ManyToOne(targetEntity="Crit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_id", referencedColumnName="id")
     * })
     */
    private $crit;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set workItemId
     *
     * @param string $workItemId
     * @return CritPremium
     */
    public function setWorkItemId($workItemId)
    {
        $this->workItemId = $workItemId;
    
        return $this;
    }

    /**
     * Get workItemId
     *
     * @return string 
     */
    public function getWorkItemId()
    {
        return $this->workItemId;
    }

    
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CritPremium
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CritPremium
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set crit
     *
     * @param Acme\SkoodioBundle\Entity\Crit $crit
     * @return CritPremium
     */
    public function setCrit(\Acme\SkoodioBundle\Entity\Crit $crit = null)
    {
        $this->crit = $crit;
    
        return $this;
    }

    /**
     * Get crit
     *
     * @return Acme\SkoodioBundle\Entity\Crit 
     */
    public function getCrit()
    {
        return $this->crit;
    }
}