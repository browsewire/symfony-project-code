<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\Response
 *
 * @ORM\Table(name="response")
 * @ORM\Entity
 */
class Response
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $responseText
     *
     * @ORM\Column(name="response_text", type="text", nullable=true)
     */
    private $responseText;

    /**
     * @var integer $isActive
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var Reviewer
     *
     * @ORM\ManyToOne(targetEntity="Reviewer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reviewer_id", referencedColumnName="id")
     * })
     */
    private $reviewer;

    /**
     * @var CritQuestion
     *
     * @ORM\ManyToOne(targetEntity="CritQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_question_id", referencedColumnName="id")
     * })
     */
    private $critQuestion;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responseText
     *
     * @param string $responseText
     * @return Response
     */
    public function setResponseText($responseText)
    {
        $this->responseText = $responseText;
    
        return $this;
    }

    /**
     * Get responseText
     *
     * @return string 
     */
    public function getResponseText()
    {
        return $this->responseText;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return Response
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Response
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Response
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set reviewer
     *
     * @param Acme\SkoodioBundle\Entity\Reviewer $reviewer
     * @return Response
     */
    public function setReviewer(\Acme\SkoodioBundle\Entity\Reviewer $reviewer = null)
    {
        $this->reviewer = $reviewer;
    
        return $this;
    }

    /**
     * Get reviewer
     *
     * @return Acme\SkoodioBundle\Entity\Reviewer 
     */
    public function getReviewer()
    {
        return $this->reviewer;
    }

    /**
     * Set critQuestion
     *
     * @param Acme\SkoodioBundle\Entity\CritQuestion $critQuestion
     * @return Response
     */
    public function setCritQuestion(\Acme\SkoodioBundle\Entity\CritQuestion $critQuestion = null)
    {
        $this->critQuestion = $critQuestion;
    
        return $this;
    }

    /**
     * Get critQuestion
     *
     * @return Acme\SkoodioBundle\Entity\CritQuestion 
     */
    public function getCritQuestion()
    {
        return $this->critQuestion;
    }
}