<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\FavouriteWork
 *
 * @ORM\Table(name="favourite_work")
 * @ORM\Entity
 */
class FavouriteWork
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   /**
     * @var integer $userId
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;
    
    
    /**
     * @var WorkItem
     *
     * @ORM\ManyToOne(targetEntity="WorkItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="work_item_id", referencedColumnName="id")
     * })
     */
    private $workItem;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    /**
     * Set userId
     *
     * @param integer $userId
     * @return FavouriteWork
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set workItem
     *
     * @param Acme\SkoodioBundle\Entity\WorkItem $workItem
     * @return Crit
     */
    public function setWorkItem(\Acme\SkoodioBundle\Entity\WorkItem $workItem = null)
    {
        $this->workItem = $workItem;
    
        return $this;
    }

    /**
     * Get workItem
     *
     * @return Acme\SkoodioBundle\Entity\WorkItem 
     */
    public function getWorkItem()
    {
        return $this->workItem;
    }
}