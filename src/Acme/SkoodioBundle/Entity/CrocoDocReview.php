<?php

namespace Acme\SkoodioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\SkoodioBundle\Entity\CrocoDocReview
 *
 * @ORM\Table(name="croco_doc_review")
 * @ORM\Entity
 */
class CrocoDocReview
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $modified
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var Reviewer
     *
     * @ORM\ManyToOne(targetEntity="Reviewer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reviewer_id", referencedColumnName="id")
     * })
     */
    private $reviewer;

    /**
     * @var Crit
     *
     * @ORM\ManyToOne(targetEntity="Crit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crit_id", referencedColumnName="id")
     * })
     */
    private $crit;

    /**
     * @var Annotation
     *
     * @ORM\ManyToOne(targetEntity="Annotation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="annotation_id", referencedColumnName="id")
     * })
     */
    private $annotation;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CrocoDocReview
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CrocoDocReview
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set reviewer
     *
     * @param Acme\SkoodioBundle\Entity\Reviewer $reviewer
     * @return CrocoDocReview
     */
    public function setReviewer(\Acme\SkoodioBundle\Entity\Reviewer $reviewer = null)
    {
        $this->reviewer = $reviewer;
    
        return $this;
    }

    /**
     * Get reviewer
     *
     * @return Acme\SkoodioBundle\Entity\Reviewer 
     */
    public function getReviewer()
    {
        return $this->reviewer;
    }

    /**
     * Set crit
     *
     * @param Acme\SkoodioBundle\Entity\Crit $crit
     * @return CrocoDocReview
     */
    public function setCrit(\Acme\SkoodioBundle\Entity\Crit $crit = null)
    {
        $this->crit = $crit;
    
        return $this;
    }

    /**
     * Get crit
     *
     * @return Acme\SkoodioBundle\Entity\Crit 
     */
    public function getCrit()
    {
        return $this->crit;
    }

    /**
     * Set annotation
     *
     * @param Acme\SkoodioBundle\Entity\Annotation $annotation
     * @return CrocoDocReview
     */
    public function setAnnotation(\Acme\SkoodioBundle\Entity\Annotation $annotation = null)
    {
        $this->annotation = $annotation;
    
        return $this;
    }

    /**
     * Get annotation
     *
     * @return Acme\SkoodioBundle\Entity\Annotation 
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }
}