<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form;
use Symfony\Component\Form\AbstractType;
use Acme\SkoodioBundle\Form\ContactType;
use Acme\SkoodioBundle\Entity\User;
use Acme\SkoodioBundle\Entity\Country;

use Acme\SkoodioBundle\Entity\WorkItem;
use Acme\SkoodioBundle\Entity\WorkItemType;
use Acme\SkoodioBundle\Entity\File;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DefaultController extends Controller
{


    // admin dashboard
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $adminsession =   $session->get('adminsession');
            
       if($adminsession)
       {
        $id = $adminsession['id'];
        $user = new User();
        $user = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->find($id);
            
        $usercount = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->findBy(array('type'=>'user','isActive'=>1));
        
        $usercount = count($usercount);
        
        $newusercount = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->findBy(array('type'=>'user','isActive'=>0));
        
        $newusercount = count($newusercount);
        
        return $this->render('AcmeAdminBundle:Default:index.html.twig', array('pagetitle'=>'Admin Dashboard','user'=>$user,'usercount'=>$usercount,'newusercount'=>$newusercount));
       }else{
        return $this->redirect($this->generateUrl('acme_admin_login')); 
       }
    }
    
    
    // admin forgot password section
     public function ajaxforgotpwdAction(Request $request)
    {
    $username=$request->request->get('username');
    $user = new User();
    $user = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->findOneBy(array('username' => $username,'type'=>'admin','isActive'=>1));
   if($user)
   {
    $random = substr(number_format(time() * rand(),0,'',''),0,10);
    $user->setPassword(md5($random));
    $email = $user->getEmail();
    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();
    $username = ucfirst($username);
    $message = \Swift_Message::newInstance()
    ->setContentType("text/html")
    ->setSubject('Forgot Password')
    ->setFrom('admin@skoodio.com')
    ->setTo($email)
    ->setBody($this->renderView('AcmeAdminBundle:Email:forgotpwd.html.twig', array('username'=>$username,'password'=>$random)));
    $this->get('mailer')->send($message);

    
        
    return new Response(json_encode('Please check email for new pasword'),200);
    }else{
     return new Response(json_encode('No such active username found:- '.$username),200);
    }
    }
    
    
    
    // sorting and pagination using ajax
    public function usersajaxAction(Request $request)
    {
        $first=$request->request->get('first');
        $order=$request->request->get('order');
        $status=$request->request->get('status');
        $last=$request->request->get('last');
                
        $userlist = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->select('user')
                            ->from('AcmeSkoodioBundle:User','user')
                            ->where('user.type = :user') 
                            ->andwhere('user.isActive = :isactive')    
                            ->setParameter('user','user')
                            ->setParameter('isactive',$status)
                            ->orderBy('user.'.$order)
                            ->setFirstResult($first)
                            ->setMaxResults($last)
                            ->getQuery()
                            ->getArrayResult();
                            
          
  
      if($userlist)    
      {
      return $this->render('AcmeAdminBundle:Default:userlistajax.html.twig',array('pagetitle'=>'All Users','userlist' => $userlist));
      }
      
     
    }
    
     public function usersajaxsearchAction(Request $request)
    {
        $first=$request->request->get('first');
        $order=$request->request->get('order');
        $status=$request->request->get('status');
        $last=$request->request->get('last');
        $keyword =$request->request->get('keyword');
                
        $userlist = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->select('user')
                            ->from('AcmeSkoodioBundle:User','user')
                            ->where('user.type = :user') 
                            ->andwhere('user.isActive = :isactive')    
                            ->setParameter('user','user')
                            ->setParameter('isactive',$status)
                            ->andwhere('user.email LIKE :keyword OR user.username LIKE :keyword OR user.firstName LIKE :keyword')    
                            ->setParameter('keyword', $keyword.'%')
                            ->orderBy('user.'.$order)
                            ->setFirstResult($first)
                            ->setMaxResults($last)
                            ->getQuery()
                            ->getArrayResult();
            
      return $this->render('AcmeAdminBundle:Default:usersajaxsearch.html.twig',array('pagetitle'=>'All Users','userlist' => $userlist));
      
      
     
    }
    
    //list all active users
    public function userlistAction()
    {
        $session = $this->getRequest()->getSession();
        $adminsession =   $session->get('adminsession');
            
       if($adminsession)
       {
        $first =0; 
        $last=$this->container->getParameter('admin_list_limit');
        $status =1;
        $userlist = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->select('user')
                            ->from('AcmeSkoodioBundle:User','user')
                            ->where('user.type = :user') 
                            ->andwhere('user.isActive = :isactive')    
                            ->setParameter('user','user')
                            ->setParameter('isactive',$status)
                            ->setFirstResult($first)
                            ->setMaxResults($last)
                            ->getQuery()
                            ->getArrayResult();
       
        return $this->render('AcmeAdminBundle:Default:userlist.html.twig',array('pagetitle'=>'All Users','userlist' => $userlist,'last'=>$last,'order'=>'id','status'=>$status));
       
       }else{
        return $this->redirect($this->generateUrl('acme_admin_login')); 
       }
    
    }
    
    
    // list user registration requests
     public function registrationrequestAction()
    {
        $session = $this->getRequest()->getSession();
        $adminsession =   $session->get('adminsession');
            
       if($adminsession)
       {
         $first =0; 
         $last=$this->container->getParameter('admin_list_limit');
         $status =0;
         $userlist = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->select('user')
                            ->from('AcmeSkoodioBundle:User','user')
                            ->where('user.type = :user') 
                            ->andwhere('user.isActive = :isactive')    
                            ->setParameter('user','user')
                            ->setParameter('isactive',$status)
                            ->setFirstResult($first)
                            ->setMaxResults($last)
                            ->getQuery()
                            ->getArrayResult();
                            
           
      
        return $this->render('AcmeAdminBundle:Default:userlist.html.twig',array('pagetitle'=>'All Registration Requests','userlist' => $userlist,'last'=>$last,'order'=>'id','status'=>$status));
      
       }else{
        return $this->redirect($this->generateUrl('acme_admin_login')); 
       }
    
    
    }
    
    
   
    // update status of users
    public function statususerAction(Request $request)
    {
        $id=$request->request->get('id');
        $status=$request->request->get('status');
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AcmeSkoodioBundle:User')->find($id);
        
        $random = substr(number_format(time() * rand(),0,'',''),0,10);
        $user->setPassword(md5($random));
        $user->setIsActive($status);
        $user->setModifyed(new \DateTime('now'));
        
        $email = $user->getEmail();
        $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        
        if($status==1)
        {
        $username =ucfirst($user->getUsername());
        $message = \Swift_Message::newInstance()
         ->setContentType("text/html")
          ->setSubject('Account Activation')
          ->setFrom('admin@skoodio.com')
          ->setTo($email)
          ->setBody($this->renderView('AcmeAdminBundle:Email:activateaccount.html.twig', array('username'=>$username,'password'=>$random)));
          $this->get('mailer')->send($message);
        }    
    
        return new Response(json_encode('user updated submitted-'.$id),200);
    }
    
    // update status of all users
    public function updateallstatusAction(Request $request)
    {
    
    $status=$request->request->get('status');
    $keyword=$request->request->get('keyword');
     if($keyword!='Enter Search Keyword')
        {
         $userupdate = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->update('AcmeSkoodioBundle:User user')
                            ->set('user.isActive', $status)
                             ->where('user.email LIKE :keyword OR user.username LIKE :keyword OR user.firstName LIKE :keyword')    
                            ->setParameter('keyword', $keyword.'%')
                            ->getQuery()
                            ->execute();
        
        }else{
     $userupdate = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->update('AcmeSkoodioBundle:User user')
                            ->set('user.isActive', $status)
                            ->where('user.type = :id')
                            ->setParameter('id','user')
                            ->getQuery()
                            ->execute();
                
     }                    
    
     return new Response(json_encode($status),200);
    }  
    
    
    
    //  Admin Login Section
    public function loginAction(Request $request)
    {
    
        $session = $this->getRequest()->getSession();
        $adminsession =   $session->get('adminsession');
            
       if(!$adminsession)
       {
        // just setup a fresh $task object (remove the dummy data)
        $user = new User();
        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('adminusername'))
        {
            $user->setUsername($cookies->get('adminusername'));
            $user->setPassword($cookies->get('adminpassword'));
            
        }
        
        $form = $this->createFormBuilder($user)
            ->add('username', 'text')
            ->add('password', 'password')
            ->getForm();
    
        if ($request->getMethod() == 'POST') 
        {
        $rememberme=$request->request->get('rememberme');
        $form->bindRequest($request);
        $username = $form['username']->getData();
        $password = md5($form['password']->getData());
        $user = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->findOneBy(array('username' => $username,'password' => $password,'type'=>'admin','isActive'=>1));
            
            

        if($user)
        {
        
            $id = $user->getId(); 
            $username = $user->getUsername();
            
            $session = $this->getRequest()->getSession();
            $session->set('adminsession', array('id'=>$id,'username'=>$username));
            $this->get('session')->setFlash('notice', 'You are logged in');
            
            
            if($rememberme==1)
            {
            $cookie1 = new Cookie('adminusername', $form['username']->getData(), time() + 3600 * 24 * 7);
            $cookie = new Cookie('adminpassword', $form['password']->getData(), time() + 3600 * 24 * 7);
            $response = new Response();
            $response->headers->setCookie($cookie1);
            $response->headers->setCookie($cookie);
            $response->send();
            }
            
            
            return $this->redirect($this->generateUrl('acme_admin_homepage'));
           
        }else{
        $this->get('session')->setFlash('notice', 'Invaldi username/password');
        return $this->render('AcmeAdminBundle:Default:login.html.twig',array('pagetitle'=>'Admin Login','form' => $form->createView()));
        }
        
        }else{
                 return $this->render('AcmeAdminBundle:Default:login.html.twig',array('pagetitle'=>'Admin Login','form' => $form->createView()));
             }
    
      }else{
      return $this->redirect($this->generateUrl('acme_admin_homepage')); 
      }
      
    }
    
    
   
    
    // user account setting page
      public function edituserAction($id , Request $request)
    {
      $session = $this->getRequest()->getSession();
      $adminsession = $session->get('adminsession');
      if(!$adminsession)
       {
       return $this->redirect($this->generateUrl('acme_admin_login')); 
       } 
       
       $user = new User();
       $user = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->find($id);
    
     $form = $this->createFormBuilder($user)
            ->add('username', 'text',array('disabled' => true))
            ->add('first_name', 'text')
            ->add('last_name', 'text')
            ->add('email', 'text')
            ->add('password', 'repeated', array(
           'first_name' => 'password',
           'second_name' => 'confirm_password',
           'type' => 'password'))
            ->getForm();
     if ($request->getMethod() == 'POST') 
        {
        $form->bindRequest($request);
        $errormessage ='';
        $email2 = $form['email']->getData();
        $checkemail = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->findOneBy(array('email' => $email2));
            
            
            if($checkemail && $checkemail->getId()!=$id )
            {
              $errormessage =  'Email already exist';
            }
            
            if($errormessage=='')
            {
            
            
            if ($form->isValid()) 
            {
            $password = $form['password']->getData();
            $username = $form['username']->getData();
            if($password!='')
            {
            $user->setPassword(md5($password));
            }
            $user->setModifyed(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->get('session')->setFlash('notice', 'Your account updated sucessfuly!');
            
            $message = \Swift_Message::newInstance()
            ->setContentType("text/html")
            ->setSubject('New Password')
            ->setFrom('admin@skoodio.com')
            ->setTo($email2)
            ->setBody($this->renderView('AcmeAdminBundle:Email:forgotpwd.html.twig', array('username'=>$username,'password'=>$password)));
            $this->get('mailer')->send($message); 
            }
            }else{
            
            $this->get('session')->setFlash('notice', 'Email already exist');
            
            }
           
        }
    
        
        return $this->render('AcmeAdminBundle:Default:edituser.html.twig',array('pagetitle'=>'Edit Account','form' => $form->createView(),'id'=>$id));
    }
    
    
    //  admin logout section
    public function logoutAction(Request $request)
    {
           $session = $this->getRequest()->getSession();
           $adminsession = $session->get('adminsession');
     
             $id = $adminsession['id'];
             $user = $this->getDoctrine()
            ->getRepository('AcmeSkoodioBundle:User')
            ->find($id);
             $user->setLastLogin(new \DateTime('now'));
             $em = $this->getDoctrine()->getManager();
             $em->persist($user);
             $em->flush();
            
            
            $session->clear('adminsession');
            
            
            return $this->redirect($this->generateUrl('acme_admin_homepage')); 
        
      
    }
    
    
    public function getuserlistAction(Request $request)
     {
        $status=$request->request->get('status');
        $first =0; 
        $last=$this->container->getParameter('admin_list_limit');
        
        $userlist = $this->getDoctrine()->getEntityManager()->createQueryBuilder()
                            ->select('user')
                            ->from('AcmeSkoodioBundle:User','user')
                            ->where('user.type = :user') 
                            ->andwhere('user.isActive = :isactive')    
                            ->setParameter('user','user')
                            ->setParameter('isactive',$status)
                            ->setFirstResult($first)
                            ->setMaxResults($last)
                            ->getQuery()
                            ->getArrayResult();
      
      
      return $this->render('AcmeAdminBundle:Default:usersajaxsearch.html.twig',array('userlist' => $userlist));
      
      
     
    
    }
    
    
   
    
}
