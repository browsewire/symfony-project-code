<?php

/**
 * TreadDesigns Controller
 *
 * @property TreadDesign $TreadDesign
 */
 
class TreadDesignsController extends AppController {

public $components = array('UploadFile');


public $paginate = array(
        'limit' => 2,
        'order' => array(
            'TreadDesign.id' => 'desc'
        )
    );
 
 
 /**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TreadDesign->recursive = 0;
		$treadDesigns = $this->paginate();
		$this->set('treadDesigns', $treadDesigns);

	
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->TreadDesign->id = $id;
		if (!$this->TreadDesign->exists()) {
			throw new NotFoundException(__('Invalid tread design'));
		}
		$this->set('treadDesign', $this->TreadDesign->read(null, $id));

		// provide a return value for Bancha requests
		return $this->TreadDesign->data;
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->data) {
	         
            $fileOK = $this->UploadFile->uploadFiles('img/Tread_designs', $this->request->data['TreadDesign']['File']);
           if(array_key_exists('urls', $fileOK)) {
          
            	// save the url in the form data
            	$this->request->data['TreadDesign']['image'] = $fileOK['urls'][0];
            }
        
			if ($this->TreadDesign->save($this->request->data['TreadDesign'])) {

					// redirect to listing if record is saved
				$this->Session->setFlash(__('The tread design has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
        // show warning if record is not saved
				$this->Session->setFlash(__('The tread design could not be saved. Please, try again.'));
			}
		}
	
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->TreadDesign->id = $id;
		if (!$this->TreadDesign->exists()) {
			throw new NotFoundException(__('Invalid tread design'));
		}
			if ($this->request->data) {
	         
            $fileOK = $this->UploadFile->uploadFiles('img/Tread_designs', $this->request->data['TreadDesign']['File']);
           if(array_key_exists('urls', $fileOK)) {
          
            	// save the url in the form data
            	$this->request->data['TreadDesign']['image'] = $fileOK['urls'][0];
            }
        
			if ($this->TreadDesign->save($this->request->data['TreadDesign'])) {
       	// redirect to listing if record is saved
				
				$this->Session->setFlash(__('The tread design has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
        // show warning if record is not saved
				$this->Session->setFlash(__('The tread design could not be saved. Please, try again.'));
			}
		}
			// Ftech record data 
      $this->request->data = $this->TreadDesign->read(null, $id);
		
		
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TreadDesign->id = $id;
		if (!$this->TreadDesign->exists()) {
			throw new NotFoundException(__('Invalid tread design'));
		}
		if ($this->TreadDesign->delete()) {

		
			$this->Session->setFlash(__('Tread design deleted'));
			$this->redirect(array('action' => 'index'));
		}

		
		$this->Session->setFlash(__('Tread design was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
    
 
   
}
