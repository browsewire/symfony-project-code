<?php
App::uses('AppModel', 'Model');
/**
 * TreadDesign Model
 *
 * @property Vendor $Vendor
 * @property Casing $Casing
 * @property MoldType $MoldType
 */
class TreadDesign extends AppModel {

	
	//public $actsAs = array('Linkable');
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tread_abb' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric'),
				'message' => 'Please enter alphanumeric value',
				'allowEmpty' => false,
			),
		),
		
		'name' => array(
            'notEmpty'=> array(
                'rule' => 'notEmpty',
                'message' => 'Name can not be blank.'
            )
            ),
		'status' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				'message' => 'Please set to active or inactive',
				'allowEmpty' => false,
			),
		),
		
	
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed


}
