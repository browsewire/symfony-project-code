<div class="actions">
    <div class="navbar">
        <div class="navbar-inner">
        <ul class="nav">
            <a class="brand" href="#">Actions</a>
            <li><?php echo $this->Html->link(__('New Tread Design'), array('action' => 'add')); ?></li>
		        <li><?php echo $this->Html->link(__('List Tread Designs'), array('action' => 'index')); ?></li>   </ul>
        </div>
    </div>
</div>
<div class="treadDesigns form">
<?php echo $this->Form->create('TreadDesign', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit Tread Design'); ?></legend>
			<?php
		echo $this->Form->input('tread_abb');
		echo $this->Form->input('name');
		echo $this->Form->input('cure_type');
		echo $this->Form->radio('status',array('1' => 'Active', '0' => 'Inactive'),array('class'=>'inputType','legend'=>false));
		echo $this->Form->file('File');
    if($this->request->data['TreadDesign']['image']!=''){
            echo '<img src="'.SITEURL.$this->request->data['TreadDesign']['image'].'" width="50" height="50" />';
        }
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
