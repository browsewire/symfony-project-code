<div class="treadDesigns view">
    
    <div class="actions">
        <div class="navbar">
            <div class="navbar-inner">
            <ul class="nav">
                <a class="brand" href="#">Actions</a>
               	<li><?php echo $this->Html->link(__('Edit Tread Design'), array('action' => 'edit', $treadDesign['TreadDesign']['id'])); ?> </li>
            		<li><?php echo $this->Form->postLink(__('Delete Tread Design'), array('action' => 'delete', $treadDesign['TreadDesign']['id']), null, __('Are you sure you want to delete # %s?', $treadDesign['TreadDesign']['id'])); ?> </li>
            		<li><?php echo $this->Html->link(__('List Tread Designs'), array('action' => 'index')); ?> </li>
            		<li><?php echo $this->Html->link(__('New Tread Design'), array('action' => 'add')); ?> </li>
		        </ul>
            </div>
        </div>
    </div>
    
	<table class="table table-bordered">
		<tr>
		<td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($treadDesign['TreadDesign']['id']); ?>
			&nbsp;
		</td>
		</tr>
		<tr>
		<td><?php echo __('Tread Abb'); ?></td>
		<td>
			<?php echo h($treadDesign['TreadDesign']['tread_abb']); ?>
			&nbsp;
		</td>
		</tr>
		<tr>
		<td><?php echo __('Name'); ?></td>
		<td>
			<?php echo h($treadDesign['TreadDesign']['name']); ?>
			&nbsp;
		</td>
		</tr>
		
		<tr>
		<td><?php echo __('Status'); ?></td>
		<td>
			<?php if($treadDesign['TreadDesign']['status']==1) { echo 'Active'; }else{ echo 'Inactive' ;} ?>
			&nbsp;
		</td>
		</tr>
		
		<tr>
		<td><?php echo __('Image'); ?></td>
		<td>
		<?php echo '<img src="'.SITEURL.$treadDesign['TreadDesign']['image'].'" width="50" height="50" />'; ?>
		
			&nbsp;
		</td>
		</tr>
	</table>
</div>


