-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 14, 2013 at 12:28 PM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `triecore`
--

-- --------------------------------------------------------

--
-- Table structure for table `tread_designs`
--

CREATE TABLE IF NOT EXISTS `tread_designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tread_abb` varchar(45) CHARACTER SET latin1 NOT NULL,
  `name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `cure_type` varchar(255) NOT NULL,
  `xref` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `vendor_id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tread_designs`
--

INSERT INTO `tread_designs` (`id`, `tread_abb`, `name`, `cure_type`, `xref`, `status`, `vendor_id`, `image`) VALUES
(12, 'dsad', 'asdasd', 'asdasd', NULL, 1, 0, 'img/Tread_designs/bg_direction_nav.png'),
(13, 'asASasASasAS', 'new design one', 'new design one', NULL, 1, 0, 'img/Tread_designs/130214115913email-icon.png'),
(14, 'sadasdasd', 'asdads', '', NULL, 0, 0, 'img/Tread_designs/bg.jpg'),
(15, 'ASDSasdA', 'sASa', '', NULL, 1, 0, 'img/Tread_designs/130214115847JoannWayWebsiteImage.jpg'),
(16, 'sdasda', 'asdasd', 'asdasd', NULL, 0, 0, 'img/Tread_designs/130214115130facebook-icon.png');
